#!/bin/bash
#Set my web page
# chkconfig: 2345 99 99
case $1 in
    'start')
            echo -e "<html>\nHello there
            <br>My IP address is $(ifconfig | grep 192 | awk '{print $2}'| sed 's/^.*://')\n
            <br>Molly
            <br>My Operating System is $(cat /etc/os-release | grep PRETTY_NAME)
            <br>The web server is $(dpkg -l | grep -i 'nginx  ' | awk '{print $2,$3}' | sed 's/^.*://')
            <br>CPU $(cat cpuinfo | grep -i 'model name' | sed 's/^.*://')</html>" >/var/www/html/index.html
            ;;
    *)
        echo "Not a valid argument"
        ;;
esac
