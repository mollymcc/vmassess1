yum -y install haproxy #installing both haproxy and nano
cp -f /vagrant/haproxy.cfg /etc/haproxy

systemctl enable haproxy
systemctl start haproxy

#Checking that HAProxy has been installed correctly
if rpm -qa | grep 'haproxy'
then
  echo "HAProxy is installed"
else
  echo "HAProxy installation failed"
  exit 1 #Exits if HAProxy install failed
fi

#'Welcome' occurs in both the nginx test page and in index.html
if curl localhost | grep 'Hello'
then
  echo "HAProxy is running correctly"
else
  echo "HAProxy is not running correctly"
fi
