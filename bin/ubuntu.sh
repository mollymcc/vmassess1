apt -y update
apt -y install nginx

cp /vagrant/index.sh /var/www/html
cp /vagrant/index.sh /etc/init.d

sudo chmod +x /etc/init.d/index.sh
sudo update-rc.d index.sh enable

if curl localhost | grep 'Hello' >/dev/null 2>&1
then
  echo "Nginx installed"
else
  echo "Nginx installation failed"
fi
