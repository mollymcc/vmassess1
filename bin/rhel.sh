#!/bin/bash
yum -y install httpd
systemctl start httpd
systemctl enable httpd

cp /vagrant/index.sh /var/www/html
cp /vagrant/index.sh /etc/init.d

sudo chmod +x /etc/init.d/index.sh
sudo chkconfig --add index.sh

#Checking that Apache httpd has been installed and that it is being run correctly
#Search for word 'Welcome' in localhost (from index.html)
if curl localhost | grep 'Hello' >/dev/null 2>&1
then
  echo "Apache installed"
else
  echo "Apache installation failed"
fi
